package oceanside.usfirst1554.youtake.data;

import java.util.ArrayList;
import java.util.Objects;

import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.SearchResultSnippet;

/**
 * A Packet Like Class that acts as a container for Video information. <br />
 * Allows qick access and use of information without much complexity. <br />
 * <br />
 * Inspired by the success of a similar "Packet" system I used in Minecraft.
 * 
 * @author Matthew Crocco (matthewcrocco@gmail.com)
 */
public class SummaryData {
	
	public static final int neededTitleLength = 50;
	
	private final int vidNum;
	private final SearchResult video;
	private final ResourceId id;
	private final String title;
	private final SearchResultSnippet snippet;
	
	public static final ArrayList<SummaryData> tracker = new ArrayList<>();
	
	public SummaryData(int id, SearchResult vid){
		this.vidNum = id;
		this.id = vid.getId();
		this.title = vid.getSnippet().getTitle();
		this.snippet = vid.getSnippet();
		this.video = vid;
		tracker.add(this);
	}
	
	/**
	 * Get Search Result Data in Packet
	 */
	public SearchResult getVideo(){
		return video;
	}

	/**
	 * Get Snippet Data in Packet
	 */
	public SearchResultSnippet getSnippet(){
		return snippet;
	}
	
	/**
	 * Get Video Number as Result -- Not Used Often
	 */
	public int getVidNum(){
		return vidNum;
	}
	
	/**
	 * Get Video Title in Packet
	 */
	public String getTitle(){
		return title;
	}
	
	/**
	 * Print VidNumber, Title, and ID in a *HOPEFULLY* nice format
	 * @return this
	 */
	public SummaryData printShortSummary(){
		System.out.println(getShortSummary());
		return this;
	}
	
	/**
	 * Get Vid Number, Title, and ID
	 */
	public String getShortSummary(){
		return "#" + vidNum+"\t"+(title.length() > neededTitleLength ? title.substring(0, neededTitleLength+1) : title.length() < neededTitleLength ? extend(title) : title) +"\t"+id.getVideoId();
	}
	
	/**
	 * Prints VidNumber, Title, Video ID, Description and Creation Date
	 * @return
	 */
	public SummaryData printFullSummary(){
		System.out.println(getFullSummary());
		return this;
	}
	
	/**
	 * Get VidNumber, Title, Video ID, Description and Creation Date
	 * @return
	 */
	public String getFullSummary(){
		return "#" + vidNum + "\t" + (title.length() > neededTitleLength ? title.substring(0, neededTitleLength+1) : title.length() < neededTitleLength ? extend(title) : title) + "\t"+id.getVideoId()+"\n"+(Objects.nonNull(snippet.getDescription()) ? "Desc: " + snippet.getDescription() + "\n\nCreated At: " + snippet.getPublishedAt().toString() : "" );
	}
		
	/**
	 * Print Quick Header
	 */
	public static void printHeader(){
		System.out.println(getHeader());
	}
	
	public static void printEnder(){
		System.out.println(getHeader().substring(getHeader().indexOf("-")));
	}
	
	/**
	 * Get Quick Header
	 * @return
	 */
	public static String getHeader(){
		return "VidNum \t\t Title \t\t\t\t\t\t VidID\n-----------------------------------------------------------------------------------------";
	}
	
	/**
	 * Print A Table of All Created Summaries in their Short Form (Number, Title, ID)
	 */
	public static void printShortTable(){
		printHeader();
		tracker.forEach(s -> s.printShortSummary());
		printEnder();
	}
	
	/**
	 * Print A Table of All Created Summaries in their Full Form (Number, Title, ID, Description, Creation Date)
	 */
	public static void printFullTable(){
		printHeader();
		tracker.forEach(s -> s.printFullSummary());
		printEnder();
	}
	
	/**
	 * Clears the Tracker List, Summaries will Still Exist BUT They will not be included in a Table Print
	 */
	public static void bleachHistory(){
		tracker.clear();
	}
	
	public static String extend(String str){
		StringBuilder sb = new StringBuilder(str);
		while(sb.length() != neededTitleLength){
			sb.append(" ");
		}
		
		return new String(sb);
	}
}
