package oceanside.usfirst1554.youtake;

import java.io.File;
import java.io.IOException; 
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.Scanner;

import oceanside.usfirst1554.youtake.data.SummaryData;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Thumbnail;
import com.google.api.services.samples.youtube.cmdline.Auth;

/**
 * Main Class of the Application. <br />
 * <br />
 * Uses Many Libraries such as the Google Data API and VGet/WGet to search for and download YouTube Videos <br />
 * This was originally created to rip a few FIRST Robotics videos off of youtube, then I polished it up. <br />
 * <br />
 * There is quite a bit of error management and Java 8 functions though this could be better. Also it needs better documentation.
 * 
 * @author Matthew Crocco (matthewcrocco@gmail.com)
 *
 */
public class YouTake {
	
	/**Summary Data of Results, Could be Empty if this Process has Not Occurred Yet*/
	public static final ArrayList<SummaryData> summary = new ArrayList<>();
	public static final String[] months = {"January","February","March","April","May","June","July","August","September",
										   "October","November","December"};
	public static final String[] supers = {"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
	
	/**Google Developer API Key, Could be Empty*/
	public static final String API_KEY = getDeveloperApiKey();
	public static final long NUM_RESULTS_LIMIT = 50;
	
	public long NUM_RESULTS = 20;
	
	public static final Scanner in = new Scanner(System.in);
	public static YouTake instance;
	private String userSeed;
	private String query;
	
	private Channel channel;
	private YouTube youtube;
	
	//TODO Quickly add a way to download again -- In Main Method with Loop and Prompt
	
	/**
	 * Cannot Be Initialized Outside of the Main Method
	 */
	private YouTake(){
		
		if(API_KEY.length() > 30) System.out.println("Google Developer API Key: " + API_KEY+"\n");
		
		try(Scanner in = new Scanner(System.in)){
			System.out.println("Usernames or ID's can be given, a Channel ID should be inputted as $ID=<ID>!");
			System.out.println("NOTE: ID's can be found in the URL of a channel. www.youtube.com/channel/<ID>\n");
			System.out.print("Channel To Search (Can Be Empty): ");
			userSeed = in.nextLine();
			userSeed = Objects.isNull(userSeed)? "" : userSeed;
			System.out.print("Search Query: ");
			query = in.nextLine();
			System.out.print("Results to Show: ");
			long results = Long.parseLong(in.nextLine());
			if(Objects.isNull(results)){
				System.err.println("Results to Show is Null! -- Defaulting to " + NUM_RESULTS);
			}else if(results > NUM_RESULTS_LIMIT){
				System.err.println("Results to Show is > "+ NUM_RESULTS_LIMIT +"! -- Defaulting to " + NUM_RESULTS_LIMIT);
				NUM_RESULTS = NUM_RESULTS_LIMIT;
			}else if(results < 0){
				System.err.println("Cannot Yield Negative Results! -- Defaulting to 0");
				NUM_RESULTS = 0;
			}else{
				NUM_RESULTS = results;
			}
			
			query = Objects.isNull(query)? "" : query;
			beginTake();
		}catch(NullPointerException e){
			System.err.println("\nERROR -- " + e.getMessage());
			System.err.println("ERROR -- Can Not Continue As Is!");
			System.err.println("ERROR -- Exiting...");
			System.exit(66605279);
		}
	}
	
	/**
	 * YouTake Entry Point
	 */
	public void beginTake(){
		
		try{
			//Build a YouTube Service with default Transport nad JSON Factory but an empty HTTPRequestInitializer -- its not needed here.
			youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, (arg0) -> {}).setApplicationName("YouTake").build();
		
			//Create Video and Channel Search Lists
			YouTube.Search.List search = youtube.search().list("id,snippet");	//Get ID and Snippet Information in PrettyPrint
			YouTube.Channels.List channels = youtube.channels().list("id,snippet,contentDetails,statistics");
			
			String apiKey = YouTake.API_KEY;	//Done Since the API_KEY Obtainment Method was added recently and I am lazy.
			
			channels.setKey(apiKey);			//Set API Key to give full access to features
			if(!userSeed.contains("$ID="))
				channels.setForUsername(userSeed);			//If Specified as a User then set the Username Filter
			else
				channels.setId(userSeed.substring(userSeed.indexOf("=") + 1));	//If Specified as a Channel, Set the Channel ID Filter
			channels.setMaxResults(5L);						//Get at Most 5 Channel Results
			ChannelListResponse chanResp = channels.execute();
			java.util.List<Channel> chanRes = chanResp.getItems();
			channel = confirmChannel(chanRes);
			
			search.setKey(apiKey);	
			search.setQ(query);				//Set Search Quest (?q= on Youtube)
			search.setType("video");		//Filter for Videos Only
			if(Objects.nonNull(channel)) search.setChannelId(channel.getId());
			search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
			search.setMaxResults(NUM_RESULTS);
			
			SearchListResponse response = search.execute();
			java.util.List<SearchResult> results = response.getItems();
			
			if(!Objects.isNull(results)){	
				if(printResults(results.iterator(), query)){		//Print Results
					YouDownloader yd = new YouDownloader(YouTake.summary).initialize();	//Prompt and Prime
					if(yd.canDownload()) yd.start();									//If The User Wants to Download, then Start the Process
					
					yd.close();															//Finalize Downloader
				}
			}
			
		}catch(GoogleJsonResponseException e){
			System.err.println("Service Error! -- " + e.getDetails().getCode() + " : " + e.getDetails().getMessage() + " : " + e.getCause() + " : " + e.getMessage());
			e.printStackTrace();
			System.exit(-2);
		}catch(IOException e){
			System.err.println("IOException -- Cause = " + e.getCause() + " -- Msg = " + e.getMessage() );
			System.exit(-3);
		}catch(Throwable t){
			t.printStackTrace();
			System.exit(-4);
		}
		
		System.out.println("Exiting Program! Thanks for Your Use!");
	}
	
	/**
	 * If there are multiple Channel Results, this method allows the user to choose the correct one. <br />
	 * This is necessary because only one Channel can be used as a filter in a search.
	 * @param list - List of Channel Results
	 * @return User Confirmed Channel or Null
	 */
	public Channel confirmChannel(java.util.List<Channel> list){
		
		for(int i = 0; i < list.size(); i++){
			Channel chan = list.get(i);
		
			//Display Info for User Review
			System.out.println("Channel as Result of " + userSeed+" -- " + (list.indexOf(chan)+1) + " of " + list.size() + ":");
				
			System.out.println("Title:\t" + chan.getSnippet().getTitle());
			System.out.println("About:\t" + chan.getSnippet().getDescription());
			System.out.println("Born: \t" + formatDate(chan.getSnippet().getPublishedAt()));								
			System.out.println("G+  : \thttps://plus.google.com/" + chan.getContentDetails().getGooglePlusUserId());		//URL to Channel Owner G+ Account, Verification for User
			System.out.println("Subs: \t" + chan.getStatistics().getSubscriberCount());
			System.out.println("Views:\t" + chan.getStatistics().getViewCount());
			System.out.println("ChanID:\t" + chan.getId());
			
			System.out.print("Is this the Channel You Want?: ");
			String inp = YouTake.in.nextLine();
				
			if(inp.equalsIgnoreCase("yes") || inp.equalsIgnoreCase("y") || inp.equalsIgnoreCase("sure") || inp.equalsIgnoreCase("neat!"))
				return chan;
			
		}
		
		return null;
		
	}
	
	/**
	 * Cleanly Print Out the Results of a YouTube Search
	 * @param iter	-- Iterator of a List of SearchResults
	 * @param query -- Original User Query
	 * @return True if Successful, else false
	 */
	public boolean printResults(Iterator<SearchResult> iter, String query){
		
		//Quick Header
		System.out.println("\n================================================================================");
		System.out.print(NUM_RESULTS + " Search Results for \"" + query + "\""+(Objects.nonNull(channel) ? " in channel \"" + channel.getSnippet().getTitle() +"\"" : "") +" -- YouTake");
		System.out.println("\n================================================================================\n");
		
		//If NO Items in Iterator or Desired Results = 0 then return Failed to prevent the Download Prompt
		if(!iter.hasNext() || NUM_RESULTS == 0){
			System.out.println("No Search Results Found!");
			return false;
		}
		
		SummaryData.bleachHistory();					//Clear the List of Previous Summaries
		YouTake.summary.clear();						//Clear the Summary List, Just in Case ;)
		int idTracker = 0;								//Tracks the Numbers Assigned to Video Results
		System.out.println("---- ");					
		while(iter.hasNext()){
			
			SearchResult result = iter.next();			//Get Next Search Result
			ResourceId id = result.getId();				//Get ID Information, Includes Channel ID and Video ID
			Thumbnail thumbs = result.getSnippet().getThumbnails().getDefault();	//A Measure to Allow Users to Check the Video Before they Download it By going to its Thumbnail Image
			
			if(id.getKind().equals("youtube#video")){	//If it IS a YouTube Video
				
				//Print Information Cleanly
				System.out.println("Vid #: \t " + (idTracker + 1));
				System.out.println("ID: \t" + id.getVideoId());
				System.out.println("Title: \t" + result.getSnippet().getTitle());
				System.out.println("Thumb: \t" + thumbs.getUrl());
				System.out.println("URL: \thttps://www.youtube.com/watch?v=" + result.getId().getVideoId());
				if(Objects.nonNull(result.getSnippet().getDescription()))	
					System.out.println("\nDesc: " + result.getSnippet().getDescription());
				System.out.println("---- ");
				
				summary.add(new SummaryData(idTracker + 1, result));	//Add This Video's Information to Summary List
				idTracker++;
			}
		}
		
		System.out.println("\n================================================================================");
		
		return true;
	}
	
	/**
	 * Take YouTube Video publishedAt().toString() from JSON and put it into the format of Month dayth, year. <br />
	 * <br />
	 * 2006-11-18 -> November 18th, 2006 <br />
	 * 2009-05-23 -> May 23rd, 2009 <br />
	 * 2012-01-21 -> January 21st, 2012 <br />
	 * @param date - A DateTime Object from a YouTube Video
	 * @return A More Clean, Readable Date Format (In My Opinion)
	 */
	private String formatDate(com.google.api.client.util.DateTime dateTime){
		
		String date = dateTime.toString();	
		date = date.substring(0, date.indexOf("T"));				//YouTube Videos Contain a T to split between Date and Time, This captures the Date Only
		
		String year = date.substring(0, date.indexOf("-"));			//2006-11-18 -> 2006
		date = date.replaceFirst("-", " ");							//2006-11-18 -> 2006 11-18
		int month = Integer.parseInt(date.substring(date.indexOf(" ")+1, date.indexOf("-"))) - 1;	//2006 11-18 -> 11
		String day = date.substring(date.indexOf("-") + 1);			//2006 11-18 -> 18
		String determiner = day.substring(day.length()-1);			//18 -> 8
		String suffix = supers[Integer.parseInt(determiner)];		//Grab the Suffix at the 8th index of supers
		
		return String.format("%s %s%s, %s", months[month], day, suffix, year);
		
	}
	
	/**
	 * Get the User Desired Results Count
	 * @return Results Count
	 */
	public long getDesiredResultsCount(){
		return NUM_RESULTS;
	}
	
	/**
	 * Get the Channel chosen by the User. May return null should a channel not be needed.
	 * @return Channel or Null if No Channel Determined
	 */
	public Channel getChannel(){
		return channel;
	}
	
	/**
	 * Get the YouTube Service Object being used throughout the Channel and Video Searches
	 * @return YouTube Service Object
	 */
	public YouTube getYouTubeService(){
		return youtube;
	}
	
	/**
	 * Get User Query
	 * @return Query used in Search
	 */
	public String getQuery(){
		return query;
	}
	
	/**
	 * Get User Seed (A Given Channel ID or Username Filter for the Search)
	 * @return User Seed/Channel Filter
	 */
	public String userSeed(){
		return userSeed;
	}
	
	/**
	 * Checks if the User Seed (Provided Channel ID or Username to Filter Results) is a Channel ID, not a Username
	 * @return true if a Channel ID, false if a Username -- This can be a non-sense name
	 */
	public boolean seedIsChannelId(){
		return (userSeed.contains("$ID=") ? true : false);
	}
	
	/**
	 * Checks if the User Seed (Provided Channel ID or Username to Filter Results) is a Username, not a Channel ID
	 * @return True if Username, False if Channel ID
	 */
	public boolean seedIsUser(){
		return !seedIsChannelId();
	}
	
	public static void main(String[] args){
		instance = new YouTake();
	}
	
	/**
	 * Get The Summary Data of All Video Results
	 * @return List of All SummaryData Available or Null if None Available
	 */
	public static ArrayList<SummaryData> getSummaries(){
		return (summary.isEmpty() ? null : summary);
	}
	
	/**
	 * Get The Google Developer API Key (Hard-Coded)
	 * @return Google Developer API Key
	 */
	public static String getApiKey(){
		return API_KEY;
	}
	
	/**
	 * Get the Hard-Coded Result Limit (Google Defines the Range as [0-50])
	 * @return Hard-Coded Results Count Limit
	 */
	public static long getResultLimit(){
		return NUM_RESULTS_LIMIT;
	}
	
	/**
	 * This is a Privacy Security Measure <br />
	 * <br />
	 * For Developers with a Google Developers API Key, this will read the Api Key from a youtube.properties file. <br />
	 * Now no one can steal your API Key and sully your name! <br /> 
	 * @return Google Developer API Key
	 */
	private static String getDeveloperApiKey(){
		File f = new File("youtube.properties");	//Properties File
		String key = "";							//Default Empty Key
		try(Scanner scan = new Scanner(f)){			//Try-With-Resources to Auto-Close the Scanner
			key = scan.nextLine();					//Get First Line of Properties file -- Should be API Key
		}catch(IOException e){					
			System.err.println("Failure To Read API Key from your youtube.properties file! Defaulting to an Empty Key!");
			System.err.println("NOTE: This Limits your Access, as a Developer, to some data.");
		}
		
		return key;
	}
	
}
