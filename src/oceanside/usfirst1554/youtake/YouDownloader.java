package oceanside.usfirst1554.youtake;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import oceanside.usfirst1554.youtake.data.SummaryData;

import com.github.axet.vget.VGet;

/**
 * Downloader Class for the YouTake Application that acts with small Builder abilities and uses VGet to download videos. <br />
 * <br />
 * Processes all the Summary Data and User Information to download videos and place them in a ~/YouTake/Videos Folder.
 * 
 * @author Matthew Crocco (matthewcrocco@gmail.com)
 */
public class YouDownloader {
	
	private long timeout = 45 * 1000;
	
	private ArrayList<SummaryData> videos;
	private Integer[] downloadIndices; 
	
	/**Default Path to Videos*/
	private static final String pathTargetDir = System.getProperty("user.home") + "/YouTake/Videos";
	
	boolean download = false;
	
	/**
	 * Constructor that takes Summary Data but will require the Selection Indices later.
	 * @param videoIds
	 */
	public YouDownloader(ArrayList<SummaryData> videoIds){
		videos = videoIds;
	}
	
	/**
	 * Constructor that takes Summary Data and Selection Indices
	 * @param videoIds
	 * @param indices
	 */
	public YouDownloader(ArrayList<SummaryData> videoIds,Integer... indices){
		videos = videoIds;
		downloadIndices = indices;
	}
	
	/**
	 * Initialize State Information and Download Indices if needed!
	 * @return this
	 */
	public YouDownloader initialize(){
		try{
			while(true){
				System.out.print("Download Videos?(yes/no): ");
				String input = YouTake.in.nextLine();
				
				if(Objects.isNull(input)){
					continue;
				}else if(input.equalsIgnoreCase("yes") || input.equalsIgnoreCase("y")){
					download = true;
					break;
				}else if(input.equalsIgnoreCase("No") || input.equalsIgnoreCase("n")){
					download = false;
					break;
				}else{
					continue;
				}
			}
			
			if(download && Objects.isNull(downloadIndices)){
				requestIndices();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return this;
	}
	
	/**
	 * Start Download Process with Timeout of timeout (45 Seconds by Default).
	 * @return this
	 * @throws IllegalStateException if the Download State was specified as False!
	 */
	public YouDownloader start(){
		
		if(!download){
			throw new IllegalStateException("It Was Stated that Download is Not Wanted!");
		}else{
		
			if(Objects.isNull(downloadIndices)){
				requestIndices();												//Get Indices if none Available
			}
			
			Arrays.asList(downloadIndices).forEach(i -> {						//For Each Index
				SummaryData summary = videos.get(i);							//Get Video at Index
				try{
					AtomicBoolean stop = new AtomicBoolean(false);				//Thread-Safe Boolean to Stop Download at Timeout
					String id = summary.getVideo().getId().getVideoId();		//Video ID
					File targetDir = new File(YouDownloader.pathTargetDir);		//Get ~/YouTake/Videos Directory
					if(!targetDir.exists()) targetDir.mkdirs();					//If Directory Does Not Exist, Create It
					
					VGet vidGetter = new VGet(new URL("https://www.youtube.com/watch?v=" + id), targetDir);
					try{
						//Thread That Forces a Timeout After 30 Seconds -- Otherwise it runs forever.
						new Thread(new Runnable(){@Override public void run(){
								long cur = System.currentTimeMillis();
								long target = cur + timeout;
								while(true){
									cur = System.currentTimeMillis();
									if(cur >= target){
										stop.set(true);
										break;
									}
								}
							}
						}).start();
						vidGetter.download(stop, ()->{}); //Assign the Thread-Safe Boolean to allow Timeouts, provide empty Runnable placeholder
					}catch(Exception e){}
					
					if(stop.get())
						System.out.println("ERROR -- Failed to Download Video #" + (i+1) + " -- " + (summary.getTitle().length() > 40 ? summary.getTitle().substring(0, 41): summary.getTitle()));
					else
						System.out.println("Finished Downloading Video #" + (i+1) + " -- " + (summary.getTitle().length() > 40 ? summary.getTitle().substring(0, 41): summary.getTitle()));
				
				}catch(IOException e){
					System.err.println("IOException -- " + e.getCause() + " -- " + e.getMessage());
					e.printStackTrace();
				}catch(SecurityException e){
					System.err.println("SecurityException -- " + e.getCause() + " -- " + e.getMessage());
					e.printStackTrace();
				}catch(Throwable t){
					t.printStackTrace();
				}
			});
		
		}
		
		
		System.out.println("\nDownloading Complete! Videos found at " + YouDownloader.pathTargetDir + "!...");
		return this;
	}
	
	/**
	 * Set Indices of Videos to Download
	 * @param indices
	 * @return this
	 */
	public YouDownloader setVideosToDownload(Integer... indices){
		downloadIndices = indices;
		return this;
	}
	
	/**
	 * Set Summary Data to Utilize
	 * @param vids
	 * @return this
	 */
	public YouDownloader setVideoIds(ArrayList<SummaryData> vids){
		videos = vids;
		return this;
	}
	
	/**
	 * Get The Indices to Be Downloaded
	 * @return indices
	 */
	public Integer[] getDownloadIndices(){
		return downloadIndices;
	}
	
	/**
	 * Get Summary Data being Utilized
	 * @return Summaries
	 */
	public ArrayList<SummaryData> getVideoIds(){
		return videos;
	}
	
	/**
	 * Finalization Method
	 */
	public void close(){
		System.out.print("Cleaning Downloader...");
		videos.clear();
		videos = null;
		if(Objects.nonNull(downloadIndices)){
			downloadIndices = null;
		}
		System.out.println(" Finished!");
		System.gc();
	}
	
	/**
	 * Prompt and Get Download Indices
	 */
	private void requestIndices(){
		
		System.out.println("\n\nQuick Summary:");
		SummaryData.printShortTable();					//Display Video Information for Selection in a nice Format
		try{
			System.out.print("\nVid Numbers To Download: ");
			String tempStr = YouTake.in.nextLine();				
			String[] input = (tempStr.contains(",") ? tempStr.split(",") : tempStr.split(" "));
			ArrayList<Integer> indexList = new ArrayList<>();
			Arrays.asList(input).forEach(s -> indexList.add(Integer.parseInt(s) - 1));	//Organize Indices and Shift Over Values to Align with Array Standards
		
			downloadIndices = indexList.toArray(new Integer[input.length]);				//Return ArrayList converted to Array
		}catch(NoSuchElementException e){
			
		}
	}
	
	/**
	 * Get Whether Or Not The Downloader can Start Downloading
	 * @return True if Yes, False if No
	 */
	public boolean canDownload(){
		return download;
	}
	
}
